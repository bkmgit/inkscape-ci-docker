FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive
ARG CI
ADD install_dependencies.sh /
RUN /install_dependencies.sh --full && rm -rf /install_dependencies.sh /var/lib/apt/lists/*
